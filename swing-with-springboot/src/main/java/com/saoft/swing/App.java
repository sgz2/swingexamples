package com.saoft.swing;

import com.saoft.swing.mvc.main.MainFrameController;
import com.saoft.swing.mvc.splash.Splash;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

import javax.swing.*;

@SpringBootApplication
public class App {

    public static void main(String[] args) {
        //先启动启动画面
        banner();

        long start = System.currentTimeMillis();
        //启动springboot
        new SwingWorker(){
            @Override
            protected Object doInBackground() {
                ConfigurableApplicationContext context = new SpringApplicationBuilder(App.class)
                                                                .headless(false)
                                                                .run(args);

                MainFrameController bean = context.getBean(MainFrameController.class);
                bean.prepareAndOpenFrame();

                long end = System.currentTimeMillis();
                System.out.println("time:"+(end-start)+"ms");
                return null;
            }
        }.execute();
    }

    private static void banner(){
        Splash splash = new Splash();
        splash.setVisible(true);
        //监听进度并更新
        ProgressBeanPostProcessor.observe().subscribe(integer -> splash.getProgress().setValue(integer)
                ,e->{}
                ,() ->splash.setVisible(false));
    }
}
